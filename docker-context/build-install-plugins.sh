#!/bin/bash

set -e

# Verificamos los plugins a installar.
GF_BUILD_INSTALL_PLUGINS="${1}"

if [ -n "${GF_BUILD_INSTALL_PLUGINS}" ]; then
  OLDIFS=$IFS
  IFS=','
  for plugin in ${GF_BUILD_INSTALL_PLUGINS}; do
    echo "Installing plugin: $plugin"
    if [[ $plugin =~ .*\;.* ]]; then
        pluginUrl=$(echo "$plugin" | cut -d';' -f 1)
        pluginWithoutUrl=$(echo "$plugin" | cut -d';' -f 2)
        grafana-cli --pluginUrl "${pluginUrl}" --pluginsDir "${GF_PATHS_PLUGINS}" plugins install ${pluginWithoutUrl}
    else
        grafana-cli --pluginsDir "${GF_PATHS_PLUGINS}" plugins install ${plugin}
    fi
  done
  IFS=$OLDIFS
fi