En estas carpetas se pueden colocar todas las imagenes que se deean customizar, por ejemplo.

* Favicon = fav32.png
* grafana logo = grafana_icon.svg
* background image = heatmap_bg_test.svg
* grafana-logo-wordmark = grafana_typelogo.svg

Aca podemos directamente copiar nuestras imagenes y hacer que la pagina de login luzca como quisieramos 

Podemos colocar directamente nuestras propias imágenes aquí y hacer que la página de inicio de sesión parezca nuestra.

[Imagenes Originales](https://github.com/grafana/grafana/tree/main/public/img)
