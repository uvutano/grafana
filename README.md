# Grafana Base con Entrypoint with Pre-commands

Permite ejecutar grafana pero previamente ejecutando comandos ubicados en /docker-entrypoint.d

## enviroments

* USER_CMD: Usuario que ejecuta el comando final del container.
* CONTAINER_ENTRYPOINT_QUIET_LOGS: No muestra los logs
* DOCKER_ENTRYPOINT_RUN_DISABLE: Cualquier valor indica que se desativa los pre comandos al arancar el container.
* PROVISIONING_ENVSUBST_TEMPLATE_DIR: Indica un directorio donde se encuentran los templates.
* PROVISIONING_ENVSUBST_TEMPLATE_SUFFIX: Indica el sufijo de los templates .template por defecto.
* PROVISIONING_ENVSUBST_OUTPUT_DIR: Directorio de escritura para los templates de provisionamiento.
